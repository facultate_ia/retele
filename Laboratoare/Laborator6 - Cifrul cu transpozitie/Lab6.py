#
# Lab 6 - Cipher with Transposition
#
# BUCUR ROBERT - ADRIAN
#
# GRUPA 10LF381
#

import re
import string
from collections import Counter

def validate_key(key : str) -> (bool, str):
    '''
    Function to verify if key contains spaces or has duplicate letters
    :param key: the key for cipher
    :return: a tuple containing the result True or False, and a message
    to show what is the problem with the key
    '''
    if re.match(r'\s', key):
        return False, 'There are spaces in key!'

    key_letters_appearance = Counter(key)
    for _, appearance in key_letters_appearance.items():
        if appearance > 1:
            return False, 'There are letters which appears more than one time!'

    return True, ''


def create_matrix(key : str, text : str) -> list:
    '''
    Function to create the matrix for encoding
    :param key: the key for cipher
    :param text: the text to be encrypted
    :return: the matrix for encryption
    '''
    sorted_key = sorted(key)
    dictionary = {}
    for letter in sorted_key:
        dictionary[letter] = sorted_key.index(letter) + 1

    matrix = [[]]
    matrix.append(key)
    matrix.remove([])
    list_indices = []
    for letter in key:
        list_indices.append(dictionary[letter])
    matrix.append(list_indices)
    
    list_letters = []
    for index in range(len(text)):
        list_letters.append(text[index])
        if len(list_letters) < len(key) and index == len(text) - 1:
            for index1 in range(0, len(key) - len(list_letters)):
                list_letters.append(string.ascii_uppercase[index1])
        if len(list_letters) == len(key):
            matrix.append(list_letters)
            list_letters = []
    
    return matrix


def print_matrix(matrix : list) -> None:
    '''
    Function to print in a nice way the matrix
    :param matrix: the matrix to be printed
    :return: None 
    '''
    for element in matrix:
        for item in element:
            print(item, ' ', end='')
        print()


def encrypt_text(matrix : list, len_matrix : int) -> str:
    '''
    Function to encrypt the text
    :param matrix: matrix for encryption
    :param len_matrix: the length of the matrix
    :return: the encrypted text
    '''
    encrypted_text = ''
    for index in range(1, len(matrix[1]) + 1):
        for number in matrix[1]:
            if number == index:
                index_tmp = matrix[1].index(number)
                for line in range(2, len_matrix):
                    encrypted_text += matrix[line][index_tmp]
                break
    return encrypted_text


def main():
    '''
    The main function
    '''
    # key = 'FLOREAA'
    # text = 'EXAMENUL CU DOMNUL FLOREA SE VA DESFASURA ONLINE'

    key = input('Please enter the key (without spaces and double letters):\n')
    result, message = validate_key(key)
    while result == False:
        print(message)
        key = input('Please enter a valid key:\n')
        result, message = validate_key(key)
    text = input('Please enter the text to be encrypted:\n')

    key = key.upper()
    text = text.upper()
    text = re.sub(r'\s', '', text)
    
    matrix = create_matrix(key, text)
    print_matrix(matrix)

    encrypted = encrypt_text(matrix, len(matrix))
    print(encrypted)


if __name__ == "__main__":
    main()