#
# Lab1 - Circle Redundancy Check (CRC)
#
# BUCUR ROBERT - ADRIAN
#
# GRUPA 10LF381
#

display_details = 'no'

def XOR(message : str, polinom : str, grade : int) -> str:
    '''
    This function makes XOR on message and polinom
    :param message: this is the message that is sent in the network
    :param polinom: this is the polinom for encoding the message
    :param grade: this is the polinom's grade
    :return: returns the modified message
    '''
    result = ''
    for index in range(grade + 1):
        if polinom[index] == message[index]:
            result += '0'
        else:
            result += '1'
    result += message[grade + 1:]
    return result

def removeZeros(message : str) -> str:
    '''
    This function removes zeros from the beginng of the message
    :param message: the message thet is sent in the network
    :return: returns the medified message
    '''
    index = 0
    while message[index] == '0':
        index += 1
    return message[index:]
    
def all_zeros(message : str) -> bool:
    '''
    This function checks if the message is full of zeros
    :param message: the message that is sent in the network
    :return: returns True if the condition is met and False else
    '''
    index = 0
    while index < len(message):
        if message[index] == '1':
            return False
        index += 1
    return True

def transform(message : str, polinom : str, grade : int) -> str:
    '''
    This function transforms the message conform to the polinom, and so encoding it
    :param message: the message that is sent in the network
    :param polinom: this is the polinom for encoding the message
    :param grade: this is the polinom's grade
    :return: returns the modified message
    '''
    result = message
    if display_details == 'yes':
        print('For the first time in transform ' + result)
    while len(result) > grade:
        result = XOR(result, polinom, grade)
        if display_details == 'yes':
            print('After XOR ' + result)
        if len(result) == len(polinom) and all_zeros(result):
            result = '0'
        else:
            result = removeZeros(result)
        if display_details == 'yes':
            print('After removing zeros ' + result)
    return result
    

def bin_add(*args):
    '''
    This function adds to binnary numbers
    :param *args: can take any numbers o arguments
    :return: returns the binnary sum
    '''
    return str(bin(sum(int(x, 2) for x in args))[2:])

def main():
    '''The main function'''
    answer = input('Do you want to display some details on running?(y/n)')
    if answer == 'y':
        display_details = 'yes'
    message = '11'
    if display_details == 'yes':
        print('The message is ' + message)
    polinom = '1011'
    if display_details == 'yes':
        print('The polinom is ' + polinom)
    grade = len(polinom) - 1
    if display_details == 'yes':
        print('The grade is ' + str(grade))
    message_prim = message
    message_prim += '0' * grade
    if display_details == 'yes':
        print('The message_prim is ' + message_prim)
    result = transform(message_prim, polinom, grade)
    T = bin_add(message_prim, result)
    if display_details == 'yes':
        print('The encoded message is ' + T)
    answer = input('Do you want to change T?(y/n)')
    if answer == 'y':
        T = T.replace('0', '1', 1)
    if answer == 'y' and display_details == 'yes':
        print('The new T is ' + T)
    if transform(T, polinom, grade) == '0':
        print('The message was sent without errors!')
    else:
        print('The message was sent with errors!')

if __name__ == '__main__':
    main()
