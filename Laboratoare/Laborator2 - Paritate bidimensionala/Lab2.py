#
# Lab2 - Two-dimensional Parity
#
# BUCUR ROBERT - ADRIAN
#
# GRUPA 10LF381
#

from typing import List

def verify(message : str) -> bool:
    '''
    This function verifies if the message length is multiple of 7
    :param message: this is the message that is send in the network
    :return: returns True if the message length is multiple of 7 and False else
    '''
    if len(message) % 7 == 0:
        return True
    else:
        return False

def make_matrix(message : str) -> List[str]:
    '''
    This function transforms the message and creates the matrix for verification
    :param message: this is the message that is send in the network
    :return: the matrix
    '''
    matrix = []
    line = ''
    for index in range(len(message)):
        line += message[index]
        index +=1
        if len(line) == 7:
            matrix.append(line)
            line = ''
    for index in range(len(matrix)):
        number_of_ones = 0
        for character in matrix[index]:
            if character == '1':
                number_of_ones += 1
        if number_of_ones % 2 == 0:
            matrix[index] += '0'
        else:
            matrix[index] += '1'
    last_line = ''
    for column in range(len(matrix[0])):
        number_of_ones = 0
        for line in range(len(matrix)):
            if matrix[line][column] == '1':
                number_of_ones += 1
        if number_of_ones % 2 == 0:
            last_line += '0'
        else:
            last_line += '1'
    matrix.append(last_line)
    return matrix
    
def make_sent_message(matrix : List[str]) -> str:
    '''
    This function creates the final message that is sent
    :param matrix: the matrix that is created from make_matrix
    :return: the final message as a string
    '''
    final_message = ''
    for item in matrix:
        final_message += item
    return final_message

def main():
    '''
    The main function
    '''
    message = '101101101000100101011'
    message_modified = '111101101000100101011'
    
    if verify(message) == False or verify(message_modified) == False:
        print('The messages are invalid!')
    else:
        matrix = make_matrix(message)
        print('The matrix is:')
        for index in matrix:
            print(index)
        string = make_sent_message(matrix)
        string2 = ''
        print('The sent message is: ')
        for index in range(len(string)):
            if index % 8 == 0 and index != 0:
                string2 += ' '
            else:
                string2 += string[index]
        print(string2)
        matrix_modified = make_matrix(message_modified)
        # string = make_sent_message(matrix)
        # print('The sent message is: ' + string)
        if matrix != matrix_modified:
            print('The message was sent with errors!')
        else:
            print('The message was sent with success!')
    
if __name__ == '__main__':
    main()
