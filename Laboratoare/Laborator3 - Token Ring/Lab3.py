#
# Lab 3 - Token Ring
#
# BUCUR ROBERT - ADRIAN
#
# GRUPA 10LF381
#

class Token:
    source_IP : str = None
    destination_IP : str = None
    sent_message : str = None
    reach_destination : bool = False
    free : bool = True
    history : [str] = []

    def __init__(self, source_IP, destination_IP, sent_message, reach_destination, free, history):
        self.source_IP = source_IP
        self.destination_IP = destination_IP
        self.sent_message = sent_message
        self.reach_destination = reach_destination
        self.free = free
        self.history = history


def receive_token(computer_IP : str, token : Token) -> None:
    '''
    Function to receive the token by a computer
    :param computer_IP: the computer to receive the token
    :param token: the token to be received
    :return: None
    '''
    token.history.append(computer_IP)
    print('*****************************************')
    print("Computer", computer_IP)
    print_token(token)
    if computer_IP == token.source_IP and token.reach_destination == True:
        print_token(token)
        token.source_IP = None
        token.destination_IP = None
        token.sent_message = None
        token.reach_destination = False
        token.free = True
        token.history = []
        return
    if token.destination_IP == computer_IP:
        print(token.sent_message)
        token.reach_destination = True


def print_token(token : Token) -> None:
    '''
    Function to print the attributes of token
    :param token: the token to display
    :return: None
    '''
    print('*****************************************')
    print('The token is characterized by the following features:')
    print('Source IP:', token.source_IP)
    print('Destination IP:', token.destination_IP)
    print('Message:', token.sent_message)
    print('Reached the destination:', token.reach_destination)
    print('Is the token free:', token.free)
    print('The history:', token.history)
    print('*****************************************')


def enter_input(list_of_computers: [str]) -> (Token, int):
    print('In the network are', len(list_of_computers), 'computers.')
    print('They are:')
    index = 0
    for computer in list_of_computers:
        print('Number of computer ', index, ' : ', computer)
        index += 1
    source_index = int(input('Enter the source (number of computer from the above list): '))
    while source_index >= len(list_of_computers) or source_index < 0:
        print('Source is not valid!')
        source_index = int(input('Enter the source (number of computer from the above list): '))
    destination_index = int(input('Enter the destination (number of computer from the above list): '))
    while destination_index >= len(list_of_computers) or destination_index < 0 or source_index == destination_index:
        print('Destination is not valid!')
        destination_index = int(input('Enter the destination (number of computer from the above list): '))
    message = input('Enter the message you\'d like to send: ')
    token = Token(list_of_computers[source_index], list_of_computers[destination_index], message, False, False, [])
    return token, source_index


def main():
    '''
    The main function
    '''
    number_of_computers = int(input('Enter the number of computers: '))
    list_of_computers = []
    for index in range(number_of_computers):
        IP = '192.168.0.' + str(index)
        list_of_computers.append(IP)
    increment = int(input('Enter the direction (-1 = desc, 1 = asc): '))
    while increment != -1 and increment != 1:
        print('You entered a wrong direction!')
        increment = int(input('Enter the direction (-1 = desc, 1 = asc)'))
    token, index = enter_input(list_of_computers)
    # print_token(token)
    while token.free == False:
        receive_token(list_of_computers[index], token)
        if token.free == True:
            answer = input('Do you want to send another message? (y/n)')
            if answer == 'y':
                token, index = enter_input(list_of_computers)
                token.free = False
                answer = ''
            else:
                return
        if index == 0 and increment < 0:
            index = len(list_of_computers) - 1
        elif index == len(list_of_computers) - 1 and increment > 0:
            index = 0
        else:
            index += increment


if __name__ == '__main__':
    main()