import java.io.File;
import java.io.FileWriter;

public class Source
{
	private String MessageToSend = "mesaj";
	public final String getMessageToSend()
	{
		return MessageToSend;
	}
	public final void setMessageToSend(String value)
	{
		MessageToSend = value;
	}

	public final void SendPackage(Package package_Renamed)
	{
		synchronized (package_Renamed)
		{
			if (IsDestinationAbleToReceive(package_Renamed))
			{
				if (IsFirstPackage(package_Renamed))
				{
					SetFirstMessage(package_Renamed);
				}
				else
				{
					if (IsPackageReceived(package_Renamed))
					{
						package_Renamed.setMessage(package_Renamed.getX() + ((Package) package_Renamed).getF() > MessageToSend.length() ? MessageToSend.substring(package_Renamed.getX()) : tangible.StringHelper.substring(MessageToSend, package_Renamed.getX(), package_Renamed.getF()));
					}
				}

				if (package_Renamed.getX() + ((Package) package_Renamed).getF() > MessageToSend.length())
				{
					package_Renamed.setFIN((byte)1);
				}

				System.out.println("Source sent the following package: " + package_Renamed);
			}
		}
	}

	private static boolean IsDestinationAbleToReceive(Package package_Renamed)
	{
		return package_Renamed.getF() != 0;
	}

	private static boolean IsFirstPackage(Package package_Renamed)
	{
		return package_Renamed.getF() == -1;
	}

	private static void SetFirstMessage(Package package_Renamed)
	{
		package_Renamed.setX(0);
		package_Renamed.setACK((byte)1);
		package_Renamed.setSYN((byte)0);
		package_Renamed.setMessage("");
	}

	/** 
		 Determines whether [is package resent] [the specified package].
	 
	 @param package The package.
	 @return 
		 <c>true</c> if [is package resent] [the specified package]; otherwise, <c>false</c>.
	 
	*/
	private static boolean IsPackageReceived(Package package_Renamed)
	{
		return package_Renamed.getACK() == 1 && package_Renamed.getSYN() == 1;
	}
}