public class Test
{
	public static void main(String argv[]) throws InterruptedException
	{
		Source source = new Source();
		Destination destination = new Destination();
		Package package_Renamed = new Package();

		while (package_Renamed.getFIN() == 0)
		{
			Thread sourceThread = new Thread()
			{
			public void run()
			{
				source.SendPackage(package_Renamed);
			}
			};
			sourceThread.start();
			sourceThread.join();

			Thread destinationThread = new Thread()
			{
			public void run()
			{
				destination.ReceivePackage(package_Renamed);
			}
			};
			destinationThread.start();
			destinationThread.join();
		}

		System.out.println(String.format("The message sent: %1$s", source.getMessageToSend()));
		System.out.println(String.format("The message received: %1$s", destination.getMessageToReceive()));
	}
}