import java.util.*;

//========================================================================
// This conversion was produced by the Free Edition of
// C# to Java Converter courtesy of Tangible Software Solutions.
// Order the Premium Edition at https://www.tangiblesoftwaresolutions.com
//========================================================================



public class Destination
{
	private String Buffer = "";
	public final String getBuffer()
	{
		return Buffer;
	}
	public final void setBuffer(String value)
	{
		Buffer = value;
	}

	private String MessageToReceive = "";
	public final String getMessageToReceive()
	{
		return MessageToReceive;
	}
	public final void setMessageToReceive(String value)
	{
		MessageToReceive = value;
	}

	public final void ReceivePackage(Package package_Renamed)
	{
		synchronized (package_Renamed)
		{
			System.out.println(String.format("Destination received the following package: %1$s", package_Renamed));

			setMessageToReceive(getMessageToReceive() + package_Renamed.getMessage());

			UpdatePackageInfo(package_Renamed);

		}
	}

	private static void UpdatePackageInfo(Package package_Renamed)
	{
		if (package_Renamed.getF() >= 0)
		{
			package_Renamed.setX(package_Renamed.getX() + package_Renamed.getF());
		}
		package_Renamed.setF(GenerateF());
		package_Renamed.setACK((byte)1);
		package_Renamed.setSYN((byte)1);
		package_Renamed.setMessage("");
	}

	private static int GenerateF()
	{
		Random rand = new Random(UUID.randomUUID().hashCode());
		return rand.nextInt(5);
	}
}