public class Package
{
	// ReSharper disable once InconsistentNaming
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: private byte ACK;
	private byte ACK;
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: public byte getACK()
	public final byte getACK()
	{
		return ACK;
	}
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: public void setACK(byte value)
	public final void setACK(byte value)
	{
		ACK = value;
	}
	// ReSharper disable once InconsistentNaming
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: private byte SYN;
	private byte SYN;
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: public byte getSYN()
	public final byte getSYN()
	{
		return SYN;
	}
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: public void setSYN(byte value)
	public final void setSYN(byte value)
	{
		SYN = value;
	}
	// ReSharper disable once InconsistentNaming
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: private byte FIN;
	private byte FIN;
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: public byte getFIN()
	public final byte getFIN()
	{
		return FIN;
	}
//C# TO JAVA CONVERTER WARNING: Unsigned integer types have no direct equivalent in Java:
//ORIGINAL LINE: public void setFIN(byte value)
	public final void setFIN(byte value)
	{
		FIN = value;
	}
	private int X;
	public final int getX()
	{
		return X;
	}
	public final void setX(int value)
	{
		X = value;
	}
	private int F;
	public final int getF()
	{
		return F;
	}
	public final void setF(int value)
	{
		F = value;
	}
	private String Message;
	public final String getMessage()
	{
		return Message;
	}
	public final void setMessage(String value)
	{
		Message = value;
	}

	public Package()
	{
		setACK((byte)0);
		setSYN((byte)0);
		setFIN((byte)0);
		setX(0);
		setF(-1);
		setMessage("");
	}

	@Override
	public String toString()
	{
		return String.format("[ACK:%1$s, SYN:%2$s, FIN:%3$s, x:%4$s, f:%5$s, Message:%6$s]", getACK(), getSYN(), getFIN(), getX(), getF(), getMessage());
	}
}