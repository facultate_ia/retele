________________________________
ULTIMA TEMA
________________________________
- generam chei
- calculam cheie de criptare
- se cunosc un nr prim si radacina sa primitiva
- Diffie Hellman:algoritm de calculare a cheilor
________________________________
q=nr prim
a=radacina primitiva a lui q
________________________________
informatii cunoscute de ambele gazde
________________________________
ex: q=7, a=3
a^1 mod q, a^2 mod q, ..., a^q-1 mod q = toate nr de la 1 la q-1
________________________________
3^1 mod 7=3
3^2 mod 7=2
3^3 mod 7=6
3^4 mod 7=4
3^5 mod 7=5
3^6 mod 7=1
_____________________________

GAZDA A 								|GAZDA B
1. generam aleator un nr strict mai mic decat nr prim [x_A]		|x_B
2. calculeaza y_A cu formula a^x_A mod q				|calculeaza y_B cu formula a^x_B mod q
3. calc A trimite lui B pe y_A						|calculatorul B trimite lui A pe y_B
4. calculeaza cheia k							|calculeaza cheia k
k=(y_B)^x_A mod q							|k=(y_A)^x_B mod q
________________________________

1. x_A =2								|1. x_B=3
2. y_A=3^2 mod 7 = 2							|2. y_B=3^3 mod 7 = 6
3. y_B=6								|3. y_A=2
4. k=6^2 mod 7=1							|4. k=2^3 mod 7=1

-------> cheia este 1.

________________________________

dupa ce obtinem cheia criptam un mesaj cu cheia respectiva
k=1 
"mesaj"--*criptare*-->nftbk
->fiecare litera sa muta cu k pozitii la dreapta
nftbk--*decriptare*-->mesaj
->fiecare litera sa muta cu k pozitii la stanga

________________________________

! Daca vreau sa criptez
k=3
"xyz"--*criptare*-->abc--*decriptare*-->xyz
//lista circulara
________________________________

