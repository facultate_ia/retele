import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
 
public class Server
{
 
    private static Socket socket;
 
    public static void main(String[] args)
    {
        try
        {
            int port = 25000;
            ServerSocket serverSocket = new ServerSocket(port);
            System.out.println("Server Started and listening to the port 25000");
 
            //Server is running always. This is done using this while(true) loop
            while(true)
            {
                //Reading the message from the client
                socket = serverSocket.accept();
                InputStream is = socket.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String number = br.readLine();
                String encriptedText = null;
				System.out.println("Message received from client is "+Decipher(encriptedText, key)); 
                //Multiplying the number by 2 and forming the return message
                String returnMessage;
                try
                {
                    int numberInIntFormat = Integer.parseInt(number);
                    int returnValue = numberInIntFormat*2;
                    returnMessage = String.valueOf(returnValue) + "\n";
                }
                catch(NumberFormatException e)
                {
                    //Input was not a number. Sending proper message back to client.
                    returnMessage = "Please send a proper number\n";
                }
 
                //Sending the response back to the client.
                OutputStream os = socket.getOutputStream();
                OutputStreamWriter osw = new OutputStreamWriter(os);
                BufferedWriter bw = new BufferedWriter(osw);
                bw.write(returnMessage);
                System.out.println("Message sent to the client is "+returnMessage);
                bw.flush();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                socket.close();
            }
            catch(Exception e){}
        }
    }
		final static String key = "pangram";


	private static int[] GetShiftIndexes(String key)
	{
		int[] indexes = new int[key.length()];
		ArrayList<Map.Entry<Integer, Character>> sortedKey = new ArrayList<Map.Entry<Integer, Character>>();

		for (int i = 0; i < key.length(); ++i)
		{
			//sortedKey.add(new Map.Entry<Integer, Character>(i, key.charAt(i)) 
		}

		Collections.sort(sortedKey, (pair1, pair2) -> pair1.getValue().compareTo(pair2.getValue()));
		{

			for (int i = 0; i < key.length(); ++i)
			indexes[sortedKey.get(i).getKey()] = i;
		}

		return indexes;
	}

	public static String Encipher(String input, String key)
	{
		input = input.length() % key.length() == 0 ? input : input + "abcdefghijklmnopqrstuvwxyz".substring(0, input.length() % key.length()); //PadRight(input.Length - input.Length % key.Length + key.Length, padChar);

		StringBuilder output = new StringBuilder();
		int totalRows = (int) Math.ceil((double) input.length() / key.length());

		char[][] rowChars = new char[totalRows][key.length()];
		char[][] colChars = new char[key.length()][totalRows];
		char[][] sortedColChars = new char[key.length()][totalRows];
		int currentRow, currentColumn;
		int[] shiftIndexes = GetShiftIndexes(key);

		for (int i = 0; i < input.length(); ++i)
		{
			currentRow = i / key.length();
			currentColumn = i % key.length();
			rowChars[currentRow][currentColumn] = input.charAt(i);
		}

		for (int i = 0; i < totalRows; ++i)
		{
		for (int j = 0; j < key.length(); ++j)
		{
			colChars[j][i] = rowChars[i][j];
		}
		}

		for (int i = 0; i < key.length(); ++i)
		{
		for (int j = 0; j < totalRows; ++j)
		{
			sortedColChars[shiftIndexes[i]][j] = colChars[i][j];
		}
		}

		for (int i = 0; i < input.length(); ++i)
		{
			currentRow = i / totalRows;
			currentColumn = i % totalRows;
			output.append(sortedColChars[currentRow][currentColumn]);
		}

		return output.toString();
	}

	public static String Decipher(String input, String key)
	{
		StringBuilder output = new StringBuilder();
		int totalColumns = (int) Math.ceil((double) input.length() / key.length());
		char[][] rowChars = new char[key.length()][totalColumns];
		char[][] colChars = new char[totalColumns][key.length()];
		char[][] unsortedColChars = new char[totalColumns][key.length()];
		int currentRow, currentColumn;
		int[] shiftIndexes = GetShiftIndexes(key);

		for (int i = 0; i < input.length(); ++i)
		{
			currentRow = i / totalColumns;
			currentColumn = i % totalColumns;
			rowChars[currentRow][currentColumn] = input.charAt(i);
		}

		for (int i = 0; i < key.length(); ++i)
		{
		for (int j = 0; j < totalColumns; ++j)
		{
			colChars[j][i] = rowChars[i][j];
		}
		}

		for (int i = 0; i < totalColumns; ++i)
		{
		for (int j = 0; j < key.length(); ++j)
		{
			unsortedColChars[i][j] = colChars[i][shiftIndexes[j]];
		}
		}

		for (int i = 0; i < input.length(); ++i)
		{
			currentRow = i / key.length();
			currentColumn = i % key.length();
			output.append(unsortedColChars[currentRow][currentColumn]);
		}

		return output.toString();
	}
}