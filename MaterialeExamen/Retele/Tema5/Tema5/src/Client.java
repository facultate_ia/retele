import java.util.*;

public class Client {

	private static void main() {
		final String key = "pangram";
		String textToEncode = new Scanner(System.in).nextLine();
		String encodedText = Encipher(textToEncode, key);

	}

	private static int[] GetShiftIndexes(String key)
	{
		int[] indexes = new int[key.length()];
		ArrayList<Map.Entry<Integer, Character>> sortedKey = new ArrayList<Map.Entry<Integer, Character>>();

		for (int i = 0; i < key.length(); ++i)
		{
			//sortedKey.add(new Map.Entry<Integer, Character>(i, key.charAt(i)));
	
			//		<Integer, Character>(i, key.charAt(i)));
		}

		Collections.sort(sortedKey, (pair1, pair2) -> pair1.getValue().compareTo(pair2.getValue()));

		for (int i = 0; i < key.length(); ++i)
		{
			indexes[sortedKey.get(i).getKey()] = i;
		}

		return indexes;
	}

	public static String Encipher(String input, String key) {
		input = input.length() % key.length() == 0 ? input
				: input + "abcdefghijklmnopqrstuvwxyz".substring(0, input.length() % key.length() + 1); // PadRight(input.Length
																										// -
																										// input.Length
																										// % key.Length
																										// + key.Length,
																										// padChar);

		StringBuilder output = new StringBuilder();
		int totalRows = (int) Math.ceil((double) input.length() / key.length());

		char[][] rowChars = new char[totalRows][key.length()];
		char[][] colChars = new char[key.length()][totalRows];
		char[][] sortedColChars = new char[key.length()][totalRows];
		int currentRow, currentColumn;
		int[] shiftIndexes = GetShiftIndexes(key);

		for (int i = 0; i < input.length(); ++i) {
			currentRow = i / key.length();
			currentColumn = i % key.length();
			rowChars[currentRow][currentColumn] = input.charAt(i);
		}

		for (int i = 0; i < totalRows; ++i) {
			for (int j = 0; j < key.length(); ++j) {
				colChars[j][i] = rowChars[i][j];
			}
		}

		for (int i = 0; i < key.length(); ++i) {
			for (int j = 0; j < totalRows; ++j) {
				sortedColChars[shiftIndexes[i]][j] = colChars[i][j];
			}
		}

		for (int i = 0; i < input.length(); ++i) {
			currentRow = i / totalRows;
			currentColumn = i % totalRows;
			output.append(sortedColChars[currentRow][currentColumn]);
		}

		return output.toString();
	}

	public static String Decipher(String input, String key) {
		StringBuilder output = new StringBuilder();
		int totalColumns = (int) Math.ceil((double) input.length() / key.length());
		char[][] rowChars = new char[key.length()][totalColumns];
		char[][] colChars = new char[totalColumns][key.length()];
		char[][] unsortedColChars = new char[totalColumns][key.length()];
		int currentRow, currentColumn;
		int[] shiftIndexes = GetShiftIndexes(key);

		for (int i = 0; i < input.length(); ++i) {
			currentRow = i / totalColumns;
			currentColumn = i % totalColumns;
			rowChars[currentRow][currentColumn] = input.charAt(i);
		}

		for (int i = 0; i < key.length(); ++i) {
			for (int j = 0; j < totalColumns; ++j) {
				colChars[j][i] = rowChars[i][j];
			}
		}

		for (int i = 0; i < totalColumns; ++i) {
			for (int j = 0; j < key.length(); ++j) {
				unsortedColChars[i][j] = colChars[i][shiftIndexes[j]];
			}
		}

		for (int i = 0; i < input.length(); ++i) {
			currentRow = i / key.length();
			currentColumn = i % key.length();
			output.append(unsortedColChars[currentRow][currentColumn]);
		}

		return output.toString();
	}
}