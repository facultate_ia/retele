
import java.util.Random;
import java.util.Scanner;

public class Problema1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String word = sc.nextLine();
		//convertirea cuvantului dat la tastatura in baza 2
		byte[] bytes = word.getBytes();
		StringBuilder binary = new StringBuilder();
		int count = 0;
		for (byte b : bytes) {
			int val = b;
			for (int i = 0; i < 8; i++) {
				binary.append((val & 128) == 0 ? 0 : 1);
				val <<= 1;
			}
			binary.append(" ");
			count++;
		}

		System.out.println(word + " to binary: " + binary);

		int[][] matrixBinary = new int[count][7];
		int k = 0, columnNumber;
		int[] line = new int[7];
		int[] column = new int[count];
		for (int i = 0; i < count; i++) {
			k++;
			columnNumber = 0;
			for (int j = 0; j < 7; j++) {
				if (binary.charAt(k) == '1') {
					matrixBinary[i][j] = 1;
					columnNumber++;
				} else {
					matrixBinary[i][j] = 0;
				}
				k++;
			}
			if (columnNumber % 2 == 0) {
				column[i] = 0;
			} else {
				column[i] = 1;
			}
			k++;
		}

		int lineNumber;
		for (int i = 0; i < 7; i++) {
			lineNumber = 0;
			for (int j = 0; j < count; j++) {
				if (matrixBinary[j][i] == 1) {
					lineNumber++;
				}
			}
			if (lineNumber % 2 == 0) {
				line[i] = 0;
			} else {
				line[i] = 1;
			}
		}

		for (int i = 0; i < count; i++) {
			for (int j = 0; j < 7; j++) {
				System.out.print(matrixBinary[i][j] + " ");
			}
			System.out.println("| " + column[i]);
		}
		System.out.println("-------------");
		for (int i = 0; i < 7; i++) {
			System.out.print(line[i] + " ");
		}
		System.out.println();

		Random rand = new Random();
		int randomIndexLine = rand.nextInt(count);
		int randomIndexColumn = rand.nextInt(7);
		if (matrixBinary[randomIndexLine][randomIndexColumn] == 0) {
			matrixBinary[randomIndexLine][randomIndexColumn] = 1;
		} else {
			matrixBinary[randomIndexLine][randomIndexColumn] = 0;
		}

		System.out.println("Dupa coruperea bitului");
		for (int i = 0; i < count; i++) {
			for (int j = 0; j < 7; j++) {
				System.out.print(matrixBinary[i][j] + " ");
			}
			System.out.println("| " + column[i]);
		}
		System.out.println("-------------");
		for (int i = 0; i < 7; i++) {
			System.out.print(line[i] + " ");
		}
		System.out.println();

		int corruptColumnIndex = 0;
		for (int i = 0; i < count; i++) {
			columnNumber = 0;
			for (int j = 0; j < 7; j++) {
				if (matrixBinary[i][j] == 1) {
					columnNumber++;
				}
			}
			if (columnNumber % 2 != column[i]) {
				corruptColumnIndex = i;
			}
		}

		int corruptLineIndex = 0;
		for (int i = 0; i < 7; i++) {
			lineNumber = 0;
			for (int j = 0; j < count; j++) {
				if (matrixBinary[j][i] == 1) {
					lineNumber++;
				}
			}
			if (lineNumber % 2 != line[i]) {
				corruptLineIndex = i;
			}
		}

		if (matrixBinary[corruptColumnIndex][corruptLineIndex] == 0) {
			matrixBinary[corruptColumnIndex][corruptLineIndex] = 1;
		} else {
			matrixBinary[corruptColumnIndex][corruptLineIndex] = 0;
		}
		System.out.println();

		System.out.println("Dupa corectarea bitului corupt");
		for (int i = 0; i < count; i++) {
			for (int j = 0; j < 7; j++) {
				System.out.print(matrixBinary[i][j] + " ");
			}
			System.out.println("| " + column[i]);
		}
		System.out.println("-------------");
		for (int i = 0; i < 7; i++) {
			System.out.print(line[i] + " ");
		}
		sc.close();
	}
}
