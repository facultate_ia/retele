
import java.util.Scanner;

public class Problema2 {
	
	private static String removeZeroFromBeginning(String word) {
		while (word.charAt(0) == '0') {
			word = word.substring(1);
		}
		return word;
	}
		//functia adauga la sfarsit 0-uri
	private static String zeroAddition(String word, int grade) {
		StringBuilder string = new StringBuilder();
		string.append(word);
		for (int i = 0; i < grade; i++) {
			string.append(0);
		}
		return string.toString();
	}

	private static int grade(String coefficient) {
		return coefficient.length() - 1;
	}

	private static void divide(String word, String coef) {
		StringBuilder string = new StringBuilder();
		if (word.length() < coef.length()) {
			if (word.isEmpty()) {
				System.out.println("Mesaj corect");
			} else {
				System.out.println("Mesaj corupt");
			}
		} else {
			for (int i = 0; i < coef.length(); i++) {
				if (word.charAt(i) == coef.charAt(i)) {
					string.append(0);
				} else {
					string.append(1);
				}
			}
			word = word.substring(coef.length());
			string.append(word);
			divide(removeZeroFromBeginning(string.toString()), coef);

		}
	}

	public static void main(String[] args) {
		System.out.println("Introduceti sirul de biti:");
		Scanner sc = new Scanner(System.in);
		String word = sc.nextLine();
		word = removeZeroFromBeginning(word);

		System.out.println("Introduceti polinomul sub forma de biti(0 sau 1):");
		String coeff = sc.nextLine();
		coeff = removeZeroFromBeginning(coeff);

		word = zeroAddition(word, grade(coeff));
		System.out.println("Sir de biti dupa ce se adauga zero-urile:");
		System.out.println(word);

		divide(word, coeff);
		sc.close();
	}
}
