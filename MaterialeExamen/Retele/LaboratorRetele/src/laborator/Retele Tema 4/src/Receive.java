package laborator.retele.mov;

import java.util.ArrayList;

public class Receive implements Runnable {
private Message message;
	private String messageReceived="";
	public Receive(Message message) {
		super();
		this.message = message;
	}
	private boolean messageHasBeenReceived;
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		synchronized (message) {
		while(message.isACK()==false) {
			try {
				wait(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(message.isFIN()==false) {
		messageReceived+=message.getMessageTransmitted();
		System.out.println("Message received!");
		message.setSYN(true);
		System.out.println(messageReceived);
		}
		}
	}
	//getters and setters
	
	public String getMessageReceived() {
		return messageReceived;
	}
	public void setMessageReceived(String messageReceived) {
		this.messageReceived = messageReceived;
	}
	public boolean isMessageHasBeenReceived() {
		return messageHasBeenReceived;
	}
	public void setMessageHasBeenReceived(boolean messageHasBeenReceived) {
		this.messageHasBeenReceived = messageHasBeenReceived;
	}
	public Message getMessage() {
		return message;
	}
	public void setMessage(Message message) {
		this.message = message;
	}

}
