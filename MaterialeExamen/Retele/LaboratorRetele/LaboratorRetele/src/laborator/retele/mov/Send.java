package laborator.retele.mov;

import java.util.Random;

public class Send implements Runnable {
	private Message message;
	

	private boolean messageSent;
	// private boolean finalWord=false;
	private String randomWord;
	Random random = new Random();

	@Override
	public void run() {
		// TODO Auto-generated method stub
		synchronized (message) {
			
		
		messageSent=false;		
		while (message != null) {
			int randomNumber=random.nextInt(message.getMessageTransmitted().length());
			randomWord = message.getMessageTransmitted().substring(randomNumber);
			message.setMessageTransmitted(message.getMessageTransmitted().substring(randomNumber, message.getMessageTransmitted().length()));;
			messageSent=true;
			message.setACK(true);
			System.out.println("Message transmitted!");
			
			}
		if(message.getMessageTransmitted()=="") {
			messageSent=false;
			message.setFIN(true);
			}
		}
	}
	public Send(Message message) {
		super();
		this.message = message;
	}

	// getters and setters
	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public boolean isMessageSent() {
		return messageSent;
	}

	public void setMessageSent(boolean messageSent) {
		this.messageSent = messageSent;
	}

	public String getRandomWord() {
		return randomWord;
	}

	public void setRandomWord(String randomWord) {
		this.randomWord = randomWord;
	}

}
