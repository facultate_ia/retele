package laborator.retele.mov;

public class Message {
	private String messageTransmitted;
	private boolean ACK;
	private boolean SYN;
	private boolean FIN;

	public Message() {
		this.ACK = false;
		this.SYN = false;
		this.FIN = false;
		this.setMessageTransmitted("Ana are mere multe si gustoase");
	}

	public Message(String messageTransmitted, boolean aCK, boolean sYN, boolean fIN) {
		super();
		
		ACK = aCK;
		SYN = sYN;
		FIN = fIN;
	}

	// getters and setters

	public boolean isACK() {
		return ACK;
	}

	public void setACK(boolean aCK) {
		ACK = aCK;
	}

	public boolean isSYN() {
		return SYN;
	}

	public void setSYN(boolean sYN) {
		SYN = sYN;
	}

	public boolean isFIN() {
		return FIN;
	}

	public void setFIN(boolean fIN) {
		FIN = fIN;
	}

	public String getMessageTransmitted() {
		return messageTransmitted;
	}

	public void setMessageTransmitted(String messageTransmitted) {
		this.messageTransmitted = messageTransmitted;
	}
}
