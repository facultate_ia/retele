package laborator.retele.tema5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Server {
    private String key;
    private String message;
    //Server server=new Server();
    public List<Character> orderedKey(String key){
        List<Character> orderedKeyChars=new ArrayList<Character>();
        for (char character:key.toCharArray()) {
            orderedKeyChars.add(character);
        }
        Collections.sort(orderedKeyChars);
       // System.out.println(orderedKeyChars);
        return orderedKeyChars;
    }

   
    public String addPadding() {
    	String alphabet ="abcdefghijklmnopqrstuvwxyz";
    	int m = message.length();
    	int k = key.length();
    	if(m%k==0)return message;
    	int nrOfLetters = k - m%k;
    	message+=alphabet.substring(0,nrOfLetters);
    	return message;
    			
    }
    //pleasetransferonemilliondollarstomyswissbankaccountsixtwotwo
    public char[][] getMatrixFromMessage(){
    	char[][] matrix=new char[(int) Math.ceil((double)message.length()/key.length())][key.length()];
    	String messageWithPadding = addPadding();    	
    	int k = key.length();
    	for(int i=0;i<Math.ceil((double)message.length()/key.length());i++) {
    		for(int j=0;j<key.length();j++) {
    			matrix[i][j]=messageWithPadding.charAt(i*k+j);
    			//message.substring(0);
    		}
    		
    	}
    	return matrix;
    }
    
    public String getEncryptedMessage() {
    	List<Character> orderedKeyChars=orderedKey(key); 
    	char[][] matrix=getMatrixFromMessage();
    	
    	String encryptedMessage="";
    	for (char c :orderedKeyChars) {
			int column=key.indexOf(c);
			for(int j=0;j<Math.ceil((double)message.length()/key.length());j++) {
				encryptedMessage+=matrix[j][column];
			}
			
		}
    	return encryptedMessage;
    }
    
    

    //constructors
    public Server() {
    }

    //getters and setters
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}