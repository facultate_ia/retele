package laborator.retele.tema5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Client {
	private String key;
	private String message;

	// Server server=new Server();
	public List<Character> getOrderedKey() {
		List<Character> orderedKeyChars = new ArrayList<Character>();
		for (char character : key.toCharArray()) {
			orderedKeyChars.add(character);
		}
		Collections.sort(orderedKeyChars);
		System.out.println(orderedKeyChars);
		return orderedKeyChars;
	}

	public String getMessageFromEncryptedMessage() {
		List<Character> orderedKeys = getOrderedKey();
		char[][] matrix = new char[(int) Math.ceil((double) message.length() / key.length())][key.length()];
		int k = key.length();
		int index=0;
		for (Character c : orderedKeys) {
			int column = key.indexOf(c);
			for(int j=0;j<Math.ceil((double)message.length()/key.length());j++) {
    			matrix[j][column]=message.charAt(index*k+j);
			}
			index++;
		}
		
		String decryptedMessage = "";
		
		for(int i=0;i<Math.ceil((double)message.length()/key.length());i++) {
    		for(int j=0;j<key.length();j++) {
    			decryptedMessage +=matrix[i][j];
    		}
    		
    	}
		return decryptedMessage;
	}

	// constructors
	public Client() {
	}

	// getters and setters
	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
