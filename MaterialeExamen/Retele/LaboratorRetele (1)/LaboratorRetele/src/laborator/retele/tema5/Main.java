package laborator.retele.tema5;

import java.util.Scanner;

public class Main {
	public static void main(String args[]) {
		Server server = new Server();
		Scanner scanner=new Scanner(System.in);
		System.out.println("Cheia:");
		String key=scanner.nextLine();
		server.setKey(key); 
		System.out.println("Mesajul:");
		String message=scanner.nextLine();
		server.setMessage(message);
		
		String encryptedMessage=  server.getEncryptedMessage();
		Client client = new Client();
		client.setMessage(encryptedMessage);
		client.setKey(key);
		
		System.out.println(client.getMessageFromEncryptedMessage());

	}

}