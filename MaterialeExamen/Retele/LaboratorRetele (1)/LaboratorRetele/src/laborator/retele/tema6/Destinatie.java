package laborator.retele.tema6;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Destinatie {

	private static String cifrulCezarInv(String mesaj, int cheie) {
		String msj = "";
		for (int i = 0; i < mesaj.length(); i++) {
			if (Character.isAlphabetic(mesaj.charAt(i))) {
				if (Character.isUpperCase(mesaj.charAt(i))) {
					msj += (char) ('A' + (mesaj.charAt(i) - 'A' - cheie) % 26);
				} else {
					msj += (char) ('a' + (mesaj.charAt(i) - 'a' - cheie) % 26);
				}
			} else
				msj += mesaj.charAt(i);
		}
		return msj;
	}

	private static int DiffieHellman(Scanner sc, DataInputStream dis, DataOutputStream dos) throws IOException {
		int p = 13, g = 9;
		System.out.println("Introduceti valoare:");
		int b = sc.nextInt();
		int y = (int) (Math.pow(g, b) % p);
		dos.writeInt(y);
		System.out.println("Y transmis");
		int x = dis.readInt();
		System.out.println("X primit");
		int k = (int) (Math.pow(x, b) % p);
//		System.out.println("K = " + k);
		return k;
	}

	public static void main(String[] args) throws IOException {
		System.out.println("--- Consola Destinatie ---\n");
		ServerSocket dss = new ServerSocket(1342);
		Socket ds = dss.accept();
		DataInputStream dis = new DataInputStream(ds.getInputStream());
		DataOutputStream dos = new DataOutputStream(ds.getOutputStream());
		Scanner sc = new Scanner(System.in);
		int cheie = DiffieHellman(sc, dis, dos);
		String mesaj = dis.readUTF();
		System.out.println("Mesaj criptat:");
		System.out.println(mesaj);
		System.out.println("Mesaj decriptat:");
		System.out.println(cifrulCezarInv(mesaj, cheie));
	}

}
